#Have the function LongestWord(sen) take the sen parameter being passed and return the largest word in the string.
#If there are two or more words that are the same length, return the first word from the string with that length.
#Ignore punctuation and assume sen will not be empty.

def LongestWord(sen)
  max_word = ""
  max_len = 0
  sen.split(" ").each do |current_word|
    word = current_word.gsub(/\W/, '')
    word_len = word.length
    if word_len > max_len
      max_len = word_len
      max_word = word
    end
  end
  max_word
end

input = "fun&!! time"

puts LongestWord(input)