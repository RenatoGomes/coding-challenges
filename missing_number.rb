#How do you find the missing number in a given integer array of 1 to 100?

current = 100
array = [63, 56, 23, 1, 46, 88, 38, 39, 35, 24, 25, 69, 68, 36, 30, 2, 6, 5, 83, 14, 13, 18, 4, 64, 28, 99, 33, 8, 47, 100, 40, 34, 82, 81, 55, 27, 73, 78, 62, 79, 94, 61, 9, 32, 95, 50, 66, 92, 98, 37, 15, 80, 48, 42, 70, 26, 85, 90, 91, 10, 29, 49, 59, 17, 71, 97, 60, 12, 86, 75, 58, 77, 20, 44, 22, 3, 65, 54, 96, 52, 31, 41, 16, 43, 21, 84, 53, 72, 74, 19, 11, 57, 67, 89, 51, 87, 45, 76, 7, 93]
array.delete_at(rand(array.length))

array.sort.reverse.each do |value|
  if value != current
    puts value
    break
  end
  current = current - 1
end